import unittest
import mock
import coverage
from .context import src

class RandomTest(unittest.TestCase):
    @mock.patch('os.urandom')
    def test_Random(self, random_mock):
        random_mock.return_value = 'aaa'
        self.assertEqual(src.Random(2), '2aaa')